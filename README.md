# Axect's GitBook

## Curriculum Vitae

### Education

* Ph.D: Dept of Physics, Yonsei University (2017 -)
* B.S: Dept of Astronomy, Yonsei University (2012 - 2016)
* Major: Particle Physics & Cosmology

### Skills

* Lang: Haskell, Rust, Julia, Elm, Python, Go
* Framework: Django, Vue
* Product: [Yonsei HEP-COSMO](http://nexus.yonsei.ac.kr)
